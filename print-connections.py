#! /usr/bin/env python
import socket
import os
import sys
import dns.resolver
import dns.reversename

if len(sys.argv) >= 4:
    res = dns.resolver.Resolver()
    res.nameservers = [sys.argv[3]]
else:
    res = dns.resolver

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.bind((sys.argv[1], int(sys.argv[2])))
sock.listen(1)
try:
    while True:
        connection = None
        try:
            connection, address = sock.accept()
            ip = address[0]
            qname = dns.reversename.from_address(ip)
            answer = res.resolve(qname, 'PTR')
            domain = str(answer[0])
            print(f"ip: {ip}, rdns: {domain}")
            connection.sendall((domain + "\n").encode())
        finally:
            if connection:
                connection.close()
finally:
    sock.close()
